# course-project


### Build and push docker image to ECR
Simple build
```
docker build --build-arg VERSION=8.1 -t dvwa:<tag-name> --load .
```
Using docker-buildx
```
docker-buildx build --build-arg VERSION=8.1 --platform linux/amd64,linux/arm/v7,linux/arm64/v8 --tag <your-account-id>.dkr.ecr.<aws-region>.amazonaws.com/dvwa:<tag-name> --push .
```

